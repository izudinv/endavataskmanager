
Pod::Spec.new do |s|
    s.name = 'UIEKit'
    s.ios.deployment_target = '12.0'
    s.version = '0.0.1'
    s.source = { :git => 'https://jasmin.com', :tag => s.version }
    s.author = { "Jasmin Ceco" => "jasmin.ceco@gmail.com" }
    s.license = 'Proprietary'
    s.homepage = 'https://jasmin.com'
    s.summary = 'UIEKit SDK'
    s.source_files = '*.{swift}', 'Source/**/**/*.{swift}'
    s.swift_versions = ['5.4'] 
  end
  
