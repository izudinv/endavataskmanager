//
//  SaveDataApplication.swift
//  CreateTask
//
//  Created by Izudin on 18. 10. 2021..
//

import Foundation
import CreateTask
import UIKit

public enum SafeDataType {
  case keychain
  case userDefaults
  case file
}



public class SaveData : SaveDataDelegate{
  public func saveData(title: String, body: String, date: Date, color: UIColor, onCompletion: @escaping () -> Void) {
    switch type {
    case .keychain:
      print("Keychain")
      onCompletion()
    case .userDefaults:
      saveToUserDefaults(title: title, body: body, date: date, color: color)
      onCompletion()
    case .file:
      print("File")
      onCompletion()
    }
  }
  
  public func saveToUserDefaults(title: String, body: String, date: Date, color : UIColor){
    
    let dateFormatterGet = ISO8601DateFormatter()
    let dateString = dateFormatterGet.string(from: date)
    
    let defaults = UserDefaults.standard

    
    var savedData = UserDefaults.standard.array(forKey: "UserDefaultsData") as? [NSDictionary]
    let appendElement : NSDictionary = ["Title" : title, "Body" : body, "Date" : dateString, "color" : color.toRGBAString()]
    
    if savedData != nil{
      savedData?.append(appendElement)
    }else{
      savedData = [appendElement]
    }
    defaults.set(savedData, forKey: "UserDefaultsData")

  }
  
  var type : SafeDataType
  
  public init(type : SafeDataType){
    self.type = type
  }
}
