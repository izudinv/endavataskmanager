//
//  AppDelegate.swift
//  EndavaTaskManager
//
//  Created by Izudin on 11. 10. 2021..
//

import UIKit
import Resolver
import ViewTask
import CreateTask
import UIEKit

import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
   let nav1 = UINavigationController()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

      window = UIWindow(frame: UIScreen.main.bounds)
      window?.rootViewController = nav1
      window?.makeKeyAndVisible()
      
      IQKeyboardManager.shared.enable = true

      Resolver.register{Router()}.implements(ViewTaskRouting.self)
      Resolver.register{SaveData(type: .userDefaults)}.implements(SaveDataDelegate.self)
      
      Resolver.resolve(ViewTaskRouting.self).showTaskTableView(navigation: nav1)
      
      return true
    }

  

}


  extension UIWindow {
      static var key: UIWindow? {
          if #available(iOS 13, *) {
              return UIApplication.shared.windows.first { $0.isKeyWindow }
          } else {
              return UIApplication.shared.keyWindow
          }
      }
  }
