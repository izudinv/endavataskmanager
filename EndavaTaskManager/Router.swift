//
//  Router.swift
//  Endava Task Manager
//
//  Created by Izudin on 12. 10. 2021..
//

import Foundation
import ViewTask
import UIKit
import CreateTask
import Resolver

public class Router : ViewTaskRouting{

  
  public func showTaskTableView(navigation: UINavigationController) {
    navigation.viewControllers = [ViewTask.build(navigation: navigation)]
  }
  
}
//
//public class CreateTaskRouterImpl : CreateTaskRouting {
//  
//  weak var navigation : UINavigationController?
//  
//  public func didFinishSavingData(){
//    print("This is  hack!!! lets tlk how to do this")
//    Resolver.resolve(ViewTaskRouting.self).showTaskTableView(navigation: navigation ?? UINavigationController())
//  }
//  
//  public init(navigation: UINavigationController){
//    self.navigation = navigation
//  }
//}
