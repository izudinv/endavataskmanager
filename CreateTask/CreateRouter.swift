//
//  CreateRouter.swift
//  CreateTask
//
//  Created by Izudin on 8. 10. 2021..
//

import Foundation
import UIKit
import Resolver

public protocol CreateTaskRouting {
  func didFinishSavingData()
  func openDetailsView(navigation: UINavigationController)
}



public struct CreateTaskFeature{
  public static func build() -> UIViewController{

    let model = CreateViewModel()
    Resolver.register{model}
   return CreateViewController()
  }
}
