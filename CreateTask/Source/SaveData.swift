//
//  SaveData.swift
//  CreateTask
//
//  Created by Izudin on 14. 10. 2021..
//

import Foundation
import UIKit

public protocol SaveDataDelegate {
  
  func saveData(title: String,body: String, date : Date, color : UIColor, onCompletion: @escaping () -> Void)
 
}

