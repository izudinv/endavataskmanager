//
//  CreateViewController.swift
//  CreateTask
//
//  Created by Izudin on 13. 10. 2021..
//

import UIKit
import Resolver
import UIEKit



class CreateViewController: UIViewController {
  
  lazy var titleField : UITextField = {
    let titleField = UITextField()
    titleField.placeholder = "Title"
    titleField.layer.cornerRadius = 10
    titleField.minimumFontSize = 32
    titleField.backgroundColor = .lightGray.withAlphaComponent(0.3)
    titleField.textAlignment = .center
    titleField.delegate = self
    return titleField
  }()
  
  lazy var bodyField : UITextField = {
    let bodyField = UITextField()
    bodyField.placeholder = "Body"
    bodyField.layer.cornerRadius = 10
    bodyField.minimumFontSize = 14
    bodyField.backgroundColor = .lightGray.withAlphaComponent(0.3)
    bodyField.textAlignment = .center
    bodyField.delegate = self
    return bodyField
  }()
  
  lazy var scheduleDate : UIDatePicker = {
    let scheduleDate = UIDatePicker()
    scheduleDate.layer.cornerRadius = 10
    scheduleDate.backgroundColor = .lightGray.withAlphaComponent(0.3)
    scheduleDate.addTarget(self, action: #selector(self.dateChanged(sender:)), for: .allEvents)
    return scheduleDate
  }()
  
  lazy var colorPickerButton : UIButton = {
    let colorPickerButton = UIButton()
    colorPickerButton.layer.cornerRadius = 10
    colorPickerButton.backgroundColor = .blue.withAlphaComponent(0.3)
    colorPickerButton.titleLabel?.textAlignment = .center
    colorPickerButton.tintColor = .black
    colorPickerButton.setTitle("Select color", for: .normal)
    return colorPickerButton
  }()
  
  lazy var submit : UIButton = {
    let submit = UIButton()
    submit.layer.cornerRadius = 10
    submit.backgroundColor = .blue.withAlphaComponent(0.3)
    submit.titleLabel?.textAlignment = .center
    submit.tintColor = .black
    submit.setTitle("Submit", for: .normal)
    return submit
  }()
  let colorPicker = UIPickerView()
  
  @Injected var model : CreateViewModel

    override func viewDidLoad() {
        super.viewDidLoad()
      view.backgroundColor = .white
    
     
      createView()
    }
  
  enum pickerData: CaseIterable{
    case red
    case blue
    case yellow
    case grey
    case purple
    case black
  }
  
    
  func createView(){

    submit.addTarget(self, action: #selector(self.submitButton), for: .touchUpInside)
    colorPickerButton.addTarget(self, action: #selector(self.pickColor), for: .touchUpInside)
    let scrollView = ScrollView {
      Stack{
        Stack(.horizontal) {
          titleField
        }.setHeight(60)
        Stack(.horizontal) {
          bodyField
        }.setHeight(60)
        Stack(.horizontal) {
          scheduleDate
        }.setHeight(120)
        colorPickerButton
        submit
      }
      .withMargins(.init(top: 16, left: 16, bottom: 0, right: 16))
      .setAlignment(.fill)
      .setDistribution(.fill)
      .setSpacing(16)
    }
    view.addSubview(scrollView)
    scrollView.fillSuperviewSafeAreaLayoutGuide()

  }
  
  @objc func dateChanged(sender : UIDatePicker){
    model.date = sender.date
  }
  
  @objc func submitButton(){
    model.submit()
  }
  
  @objc func pickColor(){
    if #available(iOS 14.0, *) {
      let colorPickerVC = UIColorPickerViewController()
      colorPickerVC.delegate = self
      present(colorPickerVC, animated: true, completion: nil)
    } else {
      colorPicker.translatesAutoresizingMaskIntoConstraints = false
      colorPicker.delegate = self
      colorPicker.dataSource = self
      colorPicker.backgroundColor = .gray
      view.addSubview(colorPicker)
      view.bringSubviewToFront(colorPicker)

      colorPicker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
      colorPicker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
      colorPicker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
      
    }
 
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
      {
          let touch = touches.first
        if touch?.view != self.colorPicker
        { self.colorPicker.removeFromSuperview() }
      }
}


extension CreateViewController : UITextFieldDelegate{

  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    if textField == bodyField{
      model.bodyText = textField.text ?? ""
    }else{
      model.titleText = textField.text ?? ""
    }
    textField.resignFirstResponder()
    return true
  }

}


extension CreateViewController : UIColorPickerViewControllerDelegate{
  
  @available(iOS 14.0, *)
  func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @available(iOS 14.0, *)
  func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
    model.color = viewController.selectedColor
    self.dismiss(animated: true, completion: nil)
  }
  
}


extension CreateViewController : UIPickerViewDelegate, UIPickerViewDataSource{
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerData.allCases.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    switch pickerData.allCases[row]{
      
    case .red:
      return "Red"
    case .blue:
      return "Blue"
    case .yellow:
      return "Yellow"
    case .grey:
      return "Grey"
    case .purple:
      return "Purple"
    case .black:
      return "Black"
    }
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    switch pickerData.allCases[row]{
      
    case .red:
      model.color = .red
    case .blue:
      model.color = .blue
    case .yellow:
      model.color = .yellow
    case .grey:
      model.color = .gray
    case .purple:
      model.color = .purple
    case .black:
      model.color = .black
    }
    print(model.color)
    
  }
  
}
