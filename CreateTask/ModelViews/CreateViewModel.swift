//
//  CreateViewModel.swift
//  CreateTask
//
//  Created by Izudin on 14. 10. 2021..
//

import Foundation
import Resolver


class CreateViewModel  {

  @Injected private var saveData : SaveDataDelegate
  @Injected private var router : CreateTaskRouting
  public var titleText = ""
  public var bodyText = ""
  public var color : UIColor = .gray
  public var date = Date()
  
  
  func submit(){
    saveData.saveData(title: titleText, body: bodyText, date: date, color: color){
      [weak self] in
      print("On save data completed")
       self?.router.didFinishSavingData()
      
    }
  }
}
