//
//  TaskModelArray.swift
//  ViewTask
//
//  Created by Izudin on 8. 11. 2021..
//

import Foundation
import UIKit

public struct TaskModelArray{
  let date : Date
  var models : [TaskModelDecoded]
  
  public init(date : Date, models : [TaskModelDecoded]){
    self.date = date
    self.models = models
  }
}
