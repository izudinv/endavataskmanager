//
//  TaskModel.swift
//  EndavaTaskManager
//
//  Created by Izudin on 18. 10. 2021..
//

import Foundation


public struct TaskModel : Codable{
  var title : String?
  var body : String?
  var color : String?
  var date : String?
  
  enum CodingKeys: String, CodingKey {
      case title = "Title"
      case body = "Body"
      case color = "color"
      case date = "Date"
  }
  
 public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    title = try container.decodeIfPresent(String.self, forKey: .title)
    body = try container.decodeIfPresent(String.self, forKey: .body)
    color = try container.decodeIfPresent(String.self, forKey: .color)
    date = try container.decodeIfPresent(String.self, forKey: .date)
  }
  
 public init(){
  }
}
