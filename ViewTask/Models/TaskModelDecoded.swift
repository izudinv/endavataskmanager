//
//  TaskModelDecoded.swift
//  ViewTask
//
//  Created by Izudin on 8. 11. 2021..
//

import Foundation
import UIKit

public struct TaskModelDecoded{
  
  var title : String?
  var body : String?
  var color : UIColor?
  var date : Date?
  
  
  public init(model : TaskModel) {
    title = model.title
    body = model.body
    color = UIColor(hexString: model.color ?? "")
    let dateFormatter = ISO8601DateFormatter()
    date = dateFormatter.date(from: model.date ?? "") ?? Date()
  }
 
   public init(){
   }
}
