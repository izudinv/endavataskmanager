//
//  ViewTaskTableViewModel.swift
//  ViewTask
//
//  Created by Izudin on 18. 10. 2021..
//

import Foundation
import UIKit
import Resolver
import SwiftUI

public class ViewTaskTableViewModel {
  
  //var tableData : [NSArray] = []
  
  func getSortedData()-> [TaskModelArray]{
    let models = prepareData()
    var modelsDecoded = getDecodedModels(models: models)
    modelsDecoded = modelsDecoded.sorted { firstModel, secondModel in
      firstModel.date ?? Date() < secondModel.date ?? Date()
    }
    
    let crossReference = Dictionary(grouping: modelsDecoded, by: {
      Calendar.current.dateComponents([.year, .month, .day], from: $0?.date ?? Date())
    })
    
    var array = crossReference.map { (key: DateComponents, value: [TaskModelDecoded]) in
      TaskModelArray(date: Calendar.current.date(from: key) ?? Date(), models: value)
    }
    
    
    array = array.sorted { firstModel, secondModel in
      firstModel.date < secondModel.date
    }
    // This works
    let today = Calendar.current.dateComponents([.year, .month, .day], from: Date())
    let date = Calendar.current.dateComponents([.year, .month, .day], from: array[0].date)
    
    if date == today{
      for i in 0...array[0].models.count - 1{
        array[0].models[i].color = .red
      }
    }
    
    
    return array
    
  }
  
  func changeColors(models: [TaskModelArray]) -> [TaskModelArray]{
    
    let today = Calendar.current.dateComponents([.year, .month, .day], from: Date())
    let date = Calendar.current.dateComponents([.year, .month, .day], from: models[0].date)
    if date == today{
      
    }
    
    return models
  }
  
  func getDecodedModels(models: [TaskModel?]?) -> [TaskModelDecoded]{
    return models?.map({ model in
      TaskModelDecoded(model: model!)
    }) ?? []
  }
  
  func prepareData() -> [TaskModel?]?{
    let dataTemp = UserDefaults.standard.array(forKey: "UserDefaultsData") as? [[String : NSObject]]
    
    return dataTemp?.map{ json in
      guard let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
      else {
        return nil
      }
      
      guard let decoded = try? JSONDecoder().decode(TaskModel.self, from: jsonData)
      else{
        return nil
      }
      return decoded
    }
    
  }
  
}
