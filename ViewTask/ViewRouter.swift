//
//  ViewRouter.swift
//  ViewTask
//
//  Created by Izudin on 8. 10. 2021..
//

import Foundation
import UIKit
import Resolver
import CreateTask

public protocol ViewTaskRouting {
  func showTaskTableView(navigation: UINavigationController)
  
}


public class ViewTask  {
  
  let model = ViewTaskTableViewModel()

  public static func build(navigation: UINavigationController) -> UIViewController {
    let vc = ViewTaskViewController(navigation: navigation)
    Resolver.register{vc}.implements(CreateTaskRouting.self)
    return vc
  }
  
}


