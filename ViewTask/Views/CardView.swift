//
//  CardView.swift
//  ViewTask
//
//  Created by Izudin on 18. 10. 2021..
//

import UIKit
import UIEKit
import SwiftUI


class CardView : UIView{
  
  private func colorView(color: UIColor) -> UIView {
    let view = UIView(backgroundColor: color)
    view.layer.cornerRadius = 10
    view.layer.masksToBounds = true
    return view
  }
  
  private func titleLabel(title: String, color : UIColor = .black) -> UILabel{
    let titleLabel = UILabel()
    titleLabel.text = title
    titleLabel.textAlignment = .center
    titleLabel.font = UIFont.systemFont(ofSize: 14)
    titleLabel.textColor = color
    return titleLabel
  }
  
  private func dateLabel(date: Date, color: UIColor) -> UIView{
    let dotView = dot(dotSize: 10, color: color)
    let dateLabel = UILabel()
    dateLabel.textAlignment = .left
    dateLabel.text = DateFormatted(wrappedValue: date).projectedValue
    return Stack(.horizontal){
      dotView
      dateLabel
    }.setDistribution(.fillProportionally)
      .setAlignment(.fill)
      .setSpacing(10)
    
  }
  
  private func dot(dotSize : Int ,color : UIColor) -> UIView{
    let containerView = UIView(frame: .init(x: 0, y: 0, width: dotSize, height: dotSize))
    let path = UIBezierPath(ovalIn: containerView.bounds)
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = path.cgPath
    shapeLayer.fillColor = color.cgColor
            
            containerView.layer.addSublayer(shapeLayer)
    return containerView
  }
  
  private func bodyLabel(body: String) -> UILabel{
    let bodyLabel = UILabel()
    bodyLabel.text = body
    return bodyLabel
  }
  
  public func returnCard2(model : CardViewer) -> Stack {
    
    return Stack {
      colorView(color: model.color)
      titleLabel(title: model.title)
      bodyLabel(body: model.body)
      dateLabel(date: model.date, color: model.color)
    } .setSpacing(16)
      .setAlignment(.fill)
      .setDistribution(.fillEqually)
  }
  
  
  public func cardFrom(title : String, body: String, color: UIColor, date: Date ) -> Stack {

   Stack{
    Stack(.horizontal){
      colorView(color: color)
    }.setHeight(40)
    Stack {
      Stack{
        titleLabel(title: title)
        bodyLabel(body: body)
      }
      .padBottom(16)
      .padTop(16)
      Stack {
        dateLabel(date: date, color: color)
      }
      .padBottom(16)
    }
    .padLeft(16)
    .padRight(16)
    }
   .setAlignment(.fill)
   .setDistribution(.fill)
   .setSpacing(16)
   .customizeShadowCard(backgroundColor: .lightGray.withAlphaComponent(0.1),
                        radiusSize: 10,
                        showShadowForCard: true)
  }
  
  public func buildCards(models : [TaskModelArray?] ) -> Stack{
      Stack {
        models.map { model in
          cardsForDate(model: model!)
        }
      }
      .setSpacing(16)
    .setAlignment(.fill)
    .setDistribution(.fillEqually)
  }
  
  func cardsForDate(model : TaskModelArray)->Stack{
    Stack{
      getDateTitle(date: model.date)
      Stack{
      model.models.map { model in
        cardFrom(title: model.title ?? "", body: model.body ?? "", color: model.color ?? .gray, date: model.date ?? Date())
      }
      }.setSpacing(8)
    }
  }
  
  func getDateTitle(date: Date)->UILabel{
    let label = UILabel()
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = DateFormatter.Style.none
    dateFormatter.dateStyle = DateFormatter.Style.medium
    let text = dateFormatter.string(from: date)
    label.text = text
    label.textAlignment = .center
    return label
  }
  
}


protocol CardViewer {
  var color : UIColor {get }
  var title : String {get }
  var body : String {get }
  var date : Date {get}
  
}


class CardViewModel : CardViewer {
  var color: UIColor {
    model.color ?? .gray
  }
  
  var title: String {
    model.title ?? ""
  }
  
  var body: String {
    model.body ?? ""
  }
  
  var date: Date {
    model.date ?? Date()
  }
  
  var model : TaskModelDecoded = TaskModelDecoded()
  
  
}


extension UIView {
    @objc func reportNoninteractiveSuperview() {
        if let sup = self.superview {
            if !sup.isUserInteractionEnabled {
                print(sup, "is disabled")
            } else {
                sup.reportNoninteractiveSuperview()
            }
        } else {
            print("no disabled superviews found")
        }
    }
}

extension UIView {
    @objc func reportSuperviews(filtering:Bool = true) {
        var currentSuper : UIView? = self.superview
        print("reporting on \(self)\n")
        while let ancestor = currentSuper {
            let ok = ancestor.bounds.contains(ancestor.convert(self.frame, from: self.superview))
            let report = "it is \(ok ? "inside" : "OUTSIDE") \(ancestor)\n"
            if !filtering || !ok { print(report) }
            currentSuper = ancestor.superview
        }
    }
}
