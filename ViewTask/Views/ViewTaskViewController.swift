//
//  ViewTaskViewController.swift
//  ViewTask
//
//  Created by Izudin on 18. 10. 2021..
//

import UIKit
import Resolver
import UIEKit
import CreateTask

class ViewTaskViewController: UIViewController, CreateTaskRouting {
  func didFinishSavingData() {
    navigation?.popViewController(animated: true)
    setupView()
  }
  
  public func openDetailsView(navigation: UINavigationController) {
    navigation.show(CreateTaskFeature.build(), sender: nil)
  }
  
  var model = ViewTaskTableViewModel()
  @LazyInjected
  var router: CreateTaskRouting
  weak var navigation : UINavigationController?
  
  convenience init(navigation: UINavigationController){
    self.init()
    self.navigation = navigation
    self.navigation?.view.backgroundColor = .white
  }
  
  lazy var button : UIButton = {
    var b = UIButton()
    b.setTitle("Create task", for: .normal)
    b.addTarget(self, action: #selector(buttonCallRelevantFunction), for: .touchUpInside)
    b.tintColor = .red
    b.backgroundColor = .blue
    return b
  }()

  @objc func buttonCallRelevantFunction(){
    router.openDetailsView(navigation: self.navigationController!)
  }
  func setupNavigationViewController(){
    
    self.navigationController?.navigationBar.backgroundColor = .white
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(addTapped))
    self.navigationController?.title = "Endava Task Manager"
  }
  
  @objc func addTapped(){
    router.openDetailsView(navigation: self.navigationController!)
  }
  
  fileprivate func setupView() {
    let models = model.getSortedData()
    
    let cardView = CardView().buildCards(models: models)
    cardView
      .setSpacing(16)
      .setDistribution(.fill)
      .setAlignment(.fill)
      .padTop(16)
      .padLeft(16)
      .padRight(16)
    
    
    if models.count  > 2 {
      //cardView.addArrangedSubview(button)
      cardView.insertArrangedSubview(getHorizontalButtons(), at: 2)
    }else{
      cardView.insertArrangedSubview(getHorizontalButtons(), at: 0)
    }
    let scrollView = ScrollView {
      cardView
    }
    self.view.addSubview(scrollView)
    scrollView.fillToSuperview()
    scrollView.clipsToBounds = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupNavigationViewController()
    setupView()
  }
  
  func getHorizontalButtons() ->Stack{
    return Stack(.horizontal){
      button

    }
  }
  
}
